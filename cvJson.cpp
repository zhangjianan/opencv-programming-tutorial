// CVJSON.CPP
#include "cvJson.h"

bool Json::cvWrite_json(std::string json_path)
{
    cv::FileStorage fs(json_path, cv::FileStorage::WRITE);

    fs << "frameCount" << 5;
    time_t rawtime;
    time(&rawtime);
    fs << "calibrationDate" << asctime(localtime(&rawtime));
    cv::Mat cameraMatrix = (cv::Mat_<double>(3,3) << 1000, 0, 320, 0, 1000, 240, 0, 0, 1);
    cv::Mat distCoeffs = (cv::Mat_<double>(5,1) << 0.1, 0.01, -0.001, 0, 0);
    fs << "cameraMatrix" << cameraMatrix << "distCoeffs" << distCoeffs;


    fs << "features" << "[";
        for(int i = 0; i < 3; i++)
        {
            int x = rand() % 640;
            int y = rand() % 480;
            uchar lbp = rand() % 256;
            fs << "{:" << "x" << x << "y" << y << "lbp" << "[:";
            for( int j = 0; j < 3; j++ )
                fs << ((lbp >> j) & 1);
            fs << "]" << "}";
        }
    fs << "]";
    fs.release();
    return true;
}

bool Json::cvRead_json(std::string json_path)
{
    cv::FileStorage fs2(json_path, cv::FileStorage::READ);
    
    // 法1: use (type) operator on FileNode.
    int frameCount = (int)fs2["frameCount"];
   
    std::string date;
    // 法2: use FileNode::operator >>
    fs2["calibrationDate"] >> date;

    cv::Mat cameraMatrix2, distCoeffs2;
    fs2["cameraMatrix"] >> cameraMatrix2;
    fs2["distCoeffs"] >> distCoeffs2;
    std::cout << "frameCount: " << frameCount << std::endl
        << "calibration date: " << date << std::endl
        << "camera matrix: " << cameraMatrix2 << std::endl
        << "distortion coeffs: " << distCoeffs2 << std::endl;
    
    cv::FileNode features = fs2["features"];
    cv::FileNodeIterator it = features.begin(), it_end = features.end();
    int idx = 0;
    std::vector<uchar> lbpval;
    // 使用 FileNodeIterator 遍历序列
    for( ; it != it_end; ++it, idx++ )
    {
        std::cout << "feature #" << idx << ": ";
        std::cout << "x=" << (int)(*it)["x"] << ", y=" << (int)(*it)["y"] << ", lbp: (";

        // 你还可以使用 FileNode>>std::vector 运算符轻松读取数值数组
        (*it)["lbp"] >> lbpval;
        for( int i = 0; i < (int)lbpval.size(); i++ )
            std::cout << " " << (int)lbpval[i];
        std::cout << ")" << std::endl;
    }
    
    fs2.release();
    return true;
}