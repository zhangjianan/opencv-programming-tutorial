// CVJSON.H
#ifndef CVJSON_H
#define CVJSON_H

#include <iostream>
#include "opencv2/core.hpp"

/**
 * @brief 可以将各种OpenCV数据结构存储到XML、YAML或[JSON]格式
**/

class Json
{
public:
    Json() = delete;
    ~Json() = delete;
    /**
     * @brief 写json文件的步骤:
     * 1) 创建新的FileStorage并打开以进行写入
     * 2) 就像使用STL流一样，使用流运算符<<写入所需的所有数据
     * 3) 使用FileStorage :: release关闭文件
    **/
    static bool cvWrite_json(std::string json_path);
    /**
     * @brief 读json文件的步骤:
     * 1) 使用FileStorage :: FileStorage构造函数或FileStorage :: open方法打开文件存储
     * 2) 读取你感兴趣的数据。使用FileStorage :: operator []，FileNode :: operator []和/或FileNodeIterator
     * 3) 使用FileStorage :: release关闭文件
    **/
    static bool cvRead_json(std::string json_path);

private:
    std::string _jsonPath = "./test.json";
};
#endif