#include "cvJson.h"

int main(int, char**)
{
    auto jsonPath = "./test.json";
    //使用opencv FileStorage写json文件
    Json::cvWrite_json(jsonPath);

    //使用opencv FileStoraged读json文件
    auto isRead = Json::cvRead_json(jsonPath);
    std::cout << isRead << std::endl;

    return 0;
}